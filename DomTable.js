import ServiceProcessInvocationsPropertiesOnHtml from "./ServiceProcessInvocationsPropertiesOnHtml.js";

export default class DomTable {

    constructor() {
        this.variable = null;
    }

    columnCustomNodes(column, dataRow, td) {

        const container = document.createElement('div'), parentNode = column.nodes && column.nodes.length > 0 ? column.nodes[0].parentNode.cloneNode(true) : null;

        container.innerHTML = parentNode.outerHTML.trim().replace("{" + this.variable + "}", JSON.stringify(dataRow).replace(/"/g, "&quot;"));

        const propertyValues = new ServiceProcessInvocationsPropertiesOnHtml({[this.variable] : dataRow}).getInvokedPropertyValues(container.innerHTML, this.variable);

        for (const propertyValue of propertyValues) {
            container.innerHTML = container.innerHTML.replaceAll('{{'+propertyValue.property+'}}', propertyValue.value);

            if (typeof this['callBackRender'] === 'function') {
                this['callBackRender'](propertyValue);
            }
        }

        const dataColumn = container.firstChild;

        for (const attribute of dataColumn.getAttributeNames()) {
            if (attribute !== 'headertext') {
                td.setAttribute(attribute, dataColumn.getAttribute(attribute));
            }
        }

        while (dataColumn.childNodes.length > 0) {
            const newChild = dataColumn.childNodes[0];
            td.appendChild(newChild);
        }

    }

    loadColumns(table, tr, columns, data, idRow, onComplete, max, scrollTop, restoreRow) {

        const timeout = setTimeout(
            () => {
                //HTML

                const columnsLength = columns.length;

                for (let index = 0; index < columnsLength; index++) {

                    if (data) {
                        const td = document.createElement("td"),
                            column = columns[index];

                        this.columnCustomNodes(column, data, td);

                        tr.appendChild(td);
                        table.appendChild(tr);

                        let tableRowsLength = table.rows.length;

                        if (tableRowsLength === max) {
                            if (index === (columns.length - 1)) {
                                if (typeof onComplete == 'function') {
                                    onComplete();
                                }

                                if (restoreRow) {
                                    table.parentNode.parentNode.scrollTop = scrollTop;
                                }
                            }
                        }
                    }
                }
                clearTimeout(timeout);
            }, 3
        );
        return timeout;

    }

    static getProperty(object, map) {

        object = JSON.parse(JSON.stringify(object));
        for (const tmp of map) {
            object = object[null != tmp ? String(tmp).trim() : ''];
        }
        if (undefined === object) {
            object = "";
        }

        return object;
    }

    setProperty(object, key, value) {

        let map = key.split('.');
        let i = 0;
        let tmpObj = object;
        for (let tmp of map) {
            if (map.length === 1) {
                tmpObj[tmp] = value;
                break;
            } else if (i === (map.length - 1)) {
                tmpObj[tmp] = value;
            } else {
                tmpObj = tmpObj[tmp];
            }
            i++;
        }
        return object;
    }

}
