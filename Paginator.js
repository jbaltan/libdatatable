export default class Paginator {

    constructor(data, numberOfRowsForPage) {

        this.dataPages = {};

        this.config = null;

        this.totalDocs = 0;

        this.data = data;
        this.numberOfRowsForPage = numberOfRowsForPage;

        let i = 0;
        for(const item of this.data) {
            item['_id'] = i;
            i++;
        }
        this.build(this.numberOfRowsForPage);

    }

    build(limitRowsForPage){

        if (!this.config){

            const totalDocs = this.data.length;

            let tmpNumberPages = totalDocs/limitRowsForPage;

            if(!Number.isInteger(tmpNumberPages)) {
                tmpNumberPages++;
            }

            this.config = {
                totalDocs,
                limitRowsForPage,
                totalPages : Math.round(tmpNumberPages)
            }
        }
    }

    selectPage(page){

        let err;

        try{

            const ids = this.getIdsForNotIn();

            if (!this.dataPages[page]) {
                const docs = [];

                const data = this.getDataNotInForIds(ids, this.config.limitRowsForPage);

                for (let doc of data) {
                    docs.push(doc);
                }
                this.dataPages[page] = docs;
            }
        }catch (e) {
            err = e;
        }

        return {
            err,
            page,
            docs : this.dataPages[page],
            totalPages : Math.round(this.config.totalDocs/this.config.limitRowsForPage),
            nextPage : (page !== this.config.totalPages),
            afterPage : (page > 1)
        }
    }

    getDataNotInForIds(ids, limit){
        const data = [];

        for(const item of this.data) {
            if(!ids.includes(item['_id'])) {
                data.push(item);
            }
        }
        data.length = limit;
        return data;
    }

    getIdsForNotIn(){
        const ids = [];
        for (const dataPage in this.dataPages) {
            for (const page of this.dataPages[dataPage]) {
                ids.push(page['_id']);
            }
        }
        return ids;
    }

}