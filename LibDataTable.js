import {DataTable} from './DataTable.js';

export default class LibDataTable {

	constructor() {

		const nodeDataTable = class extends HTMLElement {

			constructor() {
				super();
			}

			connectedCallback() {

				const trElements = this.getElementsByTagName('trElement');
				let trElement;
				if (trElements){
					trElement = trElements[0];
				}

				//build dataTable
				const dataTable = new DataTable(
					this.#buildColumnsOfTable()
					, this
					, null
					, "nodeTable" + this.id
					, this.getAttribute("style")
					, false
					, null
					, null
					, trElement
				);

				if (this.getAttribute("emptyMessage")) {
					dataTable.emptyMessage = this.getAttribute("emptyMessage");
				}

				if (this.getAttribute("var")) {
					dataTable.variable = this.getAttribute("var");
				}

				dataTable.headerBuilder();

				const value = this.getAttribute("value");

				if (value){
					this.#resolveValue(dataTable, value);
				}else{

					this.setValue = (value)=>{
						this.setAttribute("value", value);
						this.#resolveValue(dataTable, value);
					}
				}

				//style scroll
				if (!this.style.overflowY) {
					this.style.overflowY = 'auto';
				}
				if (!this.style.display) {
					this.style.display = 'block';
				}
			}

			#resolveValue(dataTable, value) {

				const nameProperties = value.split(".");
				let valueInitial = window, valueParent = valueInitial, keyFinal = nameProperties[0], isFirst = true;

				for (const [i, nameProperty] of nameProperties.entries()) {

					if(isFirst){
						isFirst = false;
						valueInitial = eval(nameProperty);
					}else{
						if(nameProperty.includes('(') && nameProperty.includes(')')){
							valueInitial = eval('valueInitial.'+nameProperty);
						}else{
							valueInitial = valueInitial[nameProperty];
						}
					}

					keyFinal = nameProperty;

					if (i < (nameProperties.length-1)){
						valueParent = valueInitial;
					}

				}

				this['__'+keyFinal+'__'] = valueParent[keyFinal];

				valueParent['__defineSetter__'](keyFinal, (value)=>{

					dataTable.dataReact = value ? value : [];

					value.push = (item) => {
						Array.prototype.push.call(value, item);
						dataTable.dataReact = value;
					};

					value.splice = (position, start) => {
						Array.prototype.splice.call(value, position, start);
						dataTable.dataReact = value;
					};

					this['__'+keyFinal+'__'] = value;
				});

				valueParent['__defineGetter__'](keyFinal, ()=>{
					return this['__'+keyFinal+'__'];
				});

				dataTable.dataReact = valueInitial;
			}

			#buildColumnsOfTable() {

				const nodesColumn = this.getElementsByTagName("data-column"),
					columns = [];

				for (const nodeColumn of nodesColumn) {

					const column = {
						nodes : nodeColumn.childNodes, attributeNames : {
							headertext : nodeColumn.getAttribute('headertext')
						}
					};
					columns.push(column);
				}

				let nodesColumnLength = nodesColumn.length;

				for (let i = 0; i < nodesColumnLength; i++) {
					this.removeChild(nodesColumn[i]);
					nodesColumnLength = nodesColumn.length;
					i--;
				}
				return columns;

			}

		}

		customElements.define('data-table', nodeDataTable);
	}

}

