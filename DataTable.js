import DomTable from './DomTable.js';
import Paginator from "./Paginator.js";
import ServiceProcessInvocationsPropertiesOnHtml from "./ServiceProcessInvocationsPropertiesOnHtml.js";

export class DataTable {

    constructor(columns, containerLocation, eventSelected, id, style, scroll, scrollDimensions, onComplete, trElement) {

        //DOM
        this.constructTable(id, style, containerLocation);
        this.divFooter = null;

        //data
        this.columns = columns;
        this.trElement = trElement;
        this.listIdRows = [];
        this.emptyMessage = "Sin Información";

        this.eventSelected = eventSelected;
        this.eventCurrent = null;

        this.filter = {
            activate: false,
            position: 'header',
            eventExternal: {
                status: false,
                method: null,
                event: null
            }
        };

        this.onComplete = onComplete;

        this.scroll = scroll;
        this.scrollDimensions = scrollDimensions;

        this.numberOfRowsForPage = 0;

        this.pagitanor = false;
        this.restoreRow = false;

        this.pageSelected = null;
        this.spanNumPagina = null;

        //auxiliaries
        this.domTable = new DomTable();

    }

    set dataReact(data) {
        this.#processUpdateData(data);
    }

    get dataReact() {
        return this.__dataReact__;
    }

    reload() {
        this.#processUpdateData(this.dataReact);
    }

    #processUpdateData(data) {
        data = JSON.parse(JSON.stringify(data));

        this.firsPage = true;
        this.bodyBuilder(data, this.onComplete, false, undefined, this.table.parentNode['scrollTop']);
        this.__dataReact__ = data;
    }

    #getColspan() {

        let i = 0;
        const columnsLength = this.columns.length;

        for (let index = 0; index < columnsLength; index++) {
            if (this.columns[index]['type'] !== 'hidden') {
                i++;
            }
        }
        return i;
    }

    constructTable(id, style, containerLocation) {

        this.style = style;

        this.table = document.createElement("table");
        this.table.id = id;
        this.table.style = style;
        this.table.className = "table table-bordered table-hover table-fixed";

        this.id = id;

        this.containerLocation = containerLocation;
    }

    constructCellsHeader(divTerciarioTable) {

        const tableHead = document.createElement("table");
        tableHead.style = this.style;

        const thead = document.createElement("thead");

        const tr = document.createElement("tr");

        const columnsLength = this.columns.length;

        for (let index = 0; index < columnsLength; index++) {

            const th = document.createElement("th"),
                column = this.columns[index],
                label = document.createElement("label");

            for (const attributeName in column.attributeNames) {
                if (attributeName !== 'headertext') {
                    th.setAttribute(attributeName, column.attributeNames[attributeName]);
                } else {
                    label.appendChild(document.createTextNode(column.attributeNames[attributeName]));
                }
            }

            th.appendChild(label);
            tr.appendChild(th);
        }

        thead.appendChild(tr);
        tableHead.appendChild(thead);

        if (this.scroll) {
            divTerciarioTable.style = "overflow:scroll; width:" + this.scrollDimensions.width + ";";
        }

        divTerciarioTable.appendChild(tableHead);

    }

    headerBuilder() {

        const divTerciarioTable = document.createElement("div");

        this.constructCellsHeader(divTerciarioTable);

        let tbody = document.createElement("tbody");
        tbody.setAttribute("id", this.table.id + "Body");
        tbody.innerHTML = '<tr><td style="font-weight: bold;" colspan="' + this.columns.length + '">' + this.emptyMessage + '</td></tr>';

        if (divTerciarioTable.children[0].offsetWidth !== 0) {
            this.table.style.width = divTerciarioTable.children[0].offsetWidth + "px";
        }
        this.table.appendChild(tbody);

        if (this.scroll) {
            divTerciarioTable.style.overflow = "scroll";
            divTerciarioTable.style.height = this.scrollDimensions.height;
            divTerciarioTable.style.widthi = this.scrollDimensions.width;
        }

        const thead = divTerciarioTable.children[0].children[0];
        divTerciarioTable.removeChild(divTerciarioTable.children[0]);
        this.table.insertBefore(thead, this.table.children[0]);

        divTerciarioTable.appendChild(this.table);
        this.containerLocation.appendChild(divTerciarioTable);

    }

    bodyBuilder(data, onComplete, isFromFilter, scrollTop) {

        data = JSON.parse(JSON.stringify(data));
        this.data = data;

        if (!isFromFilter) {
            this.dataGroup = JSON.parse(JSON.stringify(this.data));
        }

        this.listIdRows.length = 0;

        const tbody = this.table.getElementsByTagName("tbody")[0],
            dataLength = data != null ? data.length : 0;

        if (dataLength <= 0) {
            tbody.innerHTML = '<tr class="ui-widget-content ui-datatable-empty-message"><td colspan="' + this.#getColspan() + '">' + this.emptyMessage + '</td></tr>';
        } else {

            if (this.pagitanor) {
                this.processPaginator(tbody, dataLength, scrollTop, onComplete);
            } else {
                this.printRows(data, tbody, dataLength, scrollTop, onComplete);
            }
        }
    }

    processPaginator(tbody, dataLength, scrollTop, onComplete) {

        for (const child of this.table.parentNode.childNodes) {
            if (child.tagName === 'NAV') {
                child.parentNode.removeChild(child);
                break;
            }
        }

        this.paginatorDataTable = new Paginator(this.data, this.numberOfRowsForPage);

        const paginator = this.paginatorDataTable,
            nav = document.createElement("nav"),
            ul = document.createElement("ul"),
            aPrevious = document.createElement("a"),
            aNext = document.createElement("a");

        ul.style.display = 'flex';
        ul.style.paddingLeft = '0';
        ul.style.listStyle = 'none';
        ul.style.borderRadius = '.25rem';

        let currenPosition = 0;

        const liPrevious = document.createElement("li"),
            liNext = document.createElement("li");
        liPrevious.className = "page-item";
        liNext.className = "page-item";

        //aPrevious.style.color = paginator.afterPage ? "#007bff" : null;
        aPrevious.style.position = 'relative';
        aPrevious.style.display = 'block';
        aPrevious.style.padding = '.5rem .75rem';
        aPrevious.style.marginLeft = '-1px';
        aPrevious.style.lineHeight = '1.25';
        aPrevious.style.color = '#949698';

        aPrevious.style.backgroundColor = '#fff';
        aPrevious.style.border = '1px solid #dee2e6';

        aPrevious.innerText = "Anterior";

        DataTable.#createNumberPage(aNext);

        aNext.innerText = "Siguiente";

        liPrevious.appendChild(aPrevious);
        liNext.appendChild(aNext);

        liPrevious.addEventListener('click', () => {
            if (liPrevious !== ul.children[currenPosition]) {
                ul.children[currenPosition].click();
            }
        })

        liNext.addEventListener('click', () => {
            if (liNext !== ul.children[currenPosition + 2]) {
                ul.children[currenPosition + 2].click();
            }
        })

        ul.appendChild(liPrevious);
        nav.appendChild(ul);

        for (let i = 0; i < paginator.config.totalPages; i++) {
            const li = document.createElement("li");
            li.className = "page-item";

            const a = document.createElement("a");

            a.style.background = 'rgb(0, 123, 255)';
            DataTable.#createNumberPage(a);

            a.innerText = String(i + 1);

            li.appendChild(a);
            ul.appendChild(li);

            li.addEventListener('click', () => {
                for (const child of ul.children) {
                    if (child !== li) {
                        child.firstChild['style'].background = "white";
                        child.firstChild['style'].color = "rgb(0, 123, 255)";
                    }
                }
                a.style.background = "#007bff";
                a.style.color = "white";

                const configPage = paginator.selectPage(i + 1);
                if (!configPage.nextPage) {
                    aNext.style.color = '#6c757d';
                } else {
                    aNext.style.color = '#007bff';
                }

                if (!configPage.afterPage) {
                    aPrevious.style.color = '#6c757d';
                } else {
                    aPrevious.style.color = '#007bff';
                }

                this.printRows(configPage.docs, tbody, dataLength, scrollTop, onComplete);

                currenPosition = i;
            });
        }

        ul.childNodes[1].dispatchEvent(new Event('click'));

        ul.appendChild(liNext);
        nav.appendChild(ul);

        this.table.after(nav);
    }

    static #createNumberPage(a) {
        a.style.position = 'relative';
        a.style.display = 'block';
        a.style.padding = '.5rem .75rem';
        a.style.marginLeft = '-1px';
        a.style.lineHeight = '1.25';
        a.style.color = 'rgb(0, 123, 255)';
        a.style.backgroundColor = '#fff';
        a.style.border = '1px solid #dee2e6';
    }

    processTr(tr, property, value) {

        const element = document.createElement('div');
        element.innerHTML = this.trElement.outerHTML.replaceAll('{{' + property + '}}', value);

        for (const elementElement of element.firstChild['attributes']) {
            let valueFinal = element['firstChild'].getAttribute(elementElement.nodeName);
            tr.setAttribute(elementElement.nodeName, valueFinal);
            this.trElement.setAttribute(elementElement.nodeName, valueFinal);
        }
    }

    printRows(data, tbody, dataLength, scrollTop, onComplete) {

        tbody.innerHTML = '';
        const i = this.pagitanor ? this.numberOfRowsForPage : dataLength;

        this.domTable.variable = this['variable'];

        for (let index = 0; index < i; index++) {

            if (data[index] !== undefined) {

                const idRow = "row-" + index + "-" + this.id;
                this.listIdRows.push(idRow);

                const tr = document.createElement("tr");
                tr.setAttribute("id", idRow);

                let element = new DOMParser().parseFromString(this.trElement.outerHTML, "text/html")['all'][3];

                for (const attribute of element.attributes) {
                    tr.setAttribute(attribute['name'], element.getAttribute(attribute['name']));
                }

                const invocations = this.resolveExpression(this.trElement.outerHTML);

                for (let invocation of invocations) {
                    if (this['variable'] === invocation.split('.')[0]) {

                        if (!this.domTable['callBackRender']) {
                            this.domTable['callBackRender'] = (propertyValue) => {
                                if (propertyValue.property === invocation) {
                                    this.processTr(tr, propertyValue.property, propertyValue.value);
                                }
                            }
                        }
                    } else {
                        //console.log(ServiceRenderHtml.resolveExpressionProperty(invocation, {[this.variable] : this.variable}));
                        let invocationAlter = '';
                        for (const invocationElement of invocation) {
                            if (invocationElement === '('){
                                break;
                            }else{
                                invocationAlter += invocationElement;
                            }
                        }
                        //const configData = new ServiceProcessInvocationsPropertiesOnHtml(data).getInvocations(this.variable, invocation, data[index]);
                        const configData = new ServiceProcessInvocationsPropertiesOnHtml(null).getInvocations(this.variable, invocation, data[index]);
                        let value = configData.textNode;

                        const typeofInvocation = typeof eval(invocationAlter);

                        for (const configInvocation of configData.invocations) {
                            const valueInitial = configInvocation.value;
                            if (typeofInvocation === 'function' ) {
                                configInvocation.value = "'"+configInvocation.value+"'";
                            }
                            value = value.replaceAll(valueInitial, configInvocation.value);
                        }
                        this.processTr(tr, invocation, eval(value));

                        try {
                            //this.processTr(tr, invocation, eval(invocation));
                        } catch (e) {

                        }

                    }
                }

                this.domTable.loadColumns(tbody, tr, this.columns, data[index], idRow, onComplete, i, scrollTop, this.restoreRow, this.trElement);
            }
        }
        this.trElement.parentNode.removeChild(this.trElement);

        this.table.appendChild(tbody);
    }

    getInvocationExpression(html) {

        const invocations = [];
        let property = "";

        for (const [i, chartHtml] of Array.from(html).entries()) {
            const chartNextHtml = html.charAt(i + 1);
            if (chartHtml === '{' && chartNextHtml === '{') {

                for (let j = i; j < html.length; j++) {
                    const chartHtmlAlter = html.charAt(j),
                        chartNextHtmlAlter = html.charAt(j + 1);
                    if (chartHtmlAlter === '}' && chartNextHtmlAlter === '}') {
                        invocations.push(property);
                        property = "";
                        break;
                    } else {
                        if (j === i) {
                            property += html.charAt(j + 2) + html.charAt(j + 3);
                            j = j + 3;
                        } else {
                            property += chartHtmlAlter;
                        }
                    }
                }
            }
        }
        return invocations;
    }

    resolveExpression(expression) {
        return this.getInvocationExpression(expression.trim());
    }
}