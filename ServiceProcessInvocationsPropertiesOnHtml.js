import DomTable from "./DomTable.js";

export default class ServiceProcessInvocationsPropertiesOnHtml {

    constructor(dataRow) {
        this.dataRow = dataRow;
    }

    getInvokedPropertyValues(html, variable) {

        const propertyValues = [];

        html = html.trim();
        let property = "";

        for (const [i, chartHtml] of Array.from(html).entries()) {
            const chartNextHtml = html.charAt(i+1);
            if (chartHtml === '{' && chartNextHtml === '{'){

                for (let j = i; j < html.length; j++) {
                    const chartHtmlAlter = html.charAt(j), chartNextHtmlAlter = html.charAt(j+1);
                    if (chartHtmlAlter === '}' && chartNextHtmlAlter === '}') {
                        //const value = ServiceProcessInvocationsPropertiesOnHtml.resolveExpressionProperty(property, this.dataRow[variable], variable);
                        const value = ServiceProcessInvocationsPropertiesOnHtml.resolveExpressionProperty(property, this.dataRow, variable);
                        if (typeof value !== "function") {
                            propertyValues.push({property,value});
                        }
                        property = "";
                        break;
                    }else {
                        if (j === i) {
                            property += html.charAt(j+2)+html.charAt(j+3);
                            j = j+3;
                        }else{
                            property += chartHtmlAlter;
                        }
                    }
                }
            }
        }
        return propertyValues;
    }

    /*processStringHtml(html) {

        html = html.trim();
        let property = "";
        let htmlFinal = html;

        for (const [i, chartHtml] of Array.from(html).entries()) {
            const chartNextHtml = html.charAt(i+1);
            if (chartHtml === '{' && chartNextHtml === '{'){

                for (let j = i; j < html.length; j++) {
                    const chartHtmlAlter = html.charAt(j), chartNextHtmlAlter = html.charAt(j+1);
                    if (chartHtmlAlter === '}' && chartNextHtmlAlter === '}') {
                        const value = ServiceRenderHtml.resolveExpressionProperty(property, this.dataColumn);
                        if (typeof value !== "function") {
                            htmlFinal = htmlFinal.replace("{{"+property+"}}", value);
                        }
                        property = "";
                        break;
                    }else {
                        if (j === i) {
                            property += html.charAt(j+2)+html.charAt(j+3);
                            j = j+3;
                        }else{
                            property += chartHtmlAlter;
                        }
                    }
                }
            }
        }
        return htmlFinal;
    }*/

    getInvocations(variable, textNode, dataRow) {

        const config = {
            textNode,
            invocations : []
        }

        if (variable){

            const patron = /[A-Za-z0-9]/;

            let propertyInvoked = new StringBuilder(), propertiesInvoked = [];

            for (let i = config.textNode.indexOf(variable); i < config.textNode.length; i++) {
                const char = config.textNode.charAt(i);
                if (!propertiesInvoked.includes(propertyInvoked)) {
                    propertiesInvoked.push(propertyInvoked);
                }

                if (!patron.test(char) && char !== '.') {

                    const map = propertyInvoked.toString().split('.');
                    map.shift();

                    const valueProperty = DomTable.getProperty(dataRow, map);
                    config.invocations.push({property:propertyInvoked.toString(), value : valueProperty});

                    config.textNode = config.textNode.replaceAll(propertyInvoked.toString(), valueProperty);
                    propertyInvoked = new StringBuilder();
                    break;
                }
                propertyInvoked.append(char);
            }
        }
        return config;
    }

    static resolveExpressionProperty(expressionProperty, dataColumn, variable) {
        if (variable !== expressionProperty.split('.')[0]) {

            let invocationAlter = '';
            for (const invocationElement of expressionProperty) {
                if (invocationElement === '('){
                    break;
                }else{
                    invocationAlter += invocationElement;
                }
            }

            const configData = new ServiceProcessInvocationsPropertiesOnHtml(null).getInvocations(variable, expressionProperty, dataColumn[variable]);
            const typeofInvocation = typeof eval(invocationAlter);
            expressionProperty = configData.textNode;

            for (const configInvocation of configData.invocations) {
                const valueInitial = configInvocation.value;
                if (typeofInvocation === 'function' ) {
                    configInvocation.value = "'"+configInvocation.value+"'";
                }
                expressionProperty = expressionProperty.replaceAll(valueInitial, configInvocation.value);
            }
            return eval(expressionProperty);
        }else{
            const x = 'this.'+expressionProperty;
            return function() { return eval(x); }.call(dataColumn);
        }
    }

}

class StringBuilder {

    constructor() {
        this.string = '';
    }

    append(object){
        this.string += object;
        return this;
    }

    split(separator){
        return this.string.split(separator);
    }

    toString(){
        return this.string.trim();
    }

}
