export default class HelperDataTable {

    static hover(process, element, table) {

    }

    static changeColorRow(rowTr, table) {

        for (let index = 0; index < table.rows.length; index++) {
            if (rowTr.id !== table.rows[index].id) {
                table.rows[index].style.background = '';
                table.rows[index].style.color = 'black';
            }
        }

        const inputs = rowTr.getElementsByTagName("input");
        const inputsLength = inputs.length;

        for (let index2 = 0; index2 < inputsLength; index2++) {
            inputs[index2].style.color = "black";
        }

        rowTr.style.background = "#0485CA";
        rowTr.style.color = "white";

    }

}